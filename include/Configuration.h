#ifndef CONFIGURATION_H
#define CONFIGURATION_H

/*
===============================================================================
    CONFIGURATION DEPENDENCIES
===============================================================================
*/

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "Macros.h"

/*
===============================================================================
    CONFIGURATION DEPENDENCIES
===============================================================================
*/

/*
    Message
    |==========|=============|==============|-|================|
    | Preamble | Control Sum | Message Type |-|                |
    |==========|=============|==============|-|  Message Data  |
    | 2 BITS   | 2 BITS      | 1 BIT        |-|                |
    |==========|=============|==============|-|================|

    Static Message Typed Data
    |=============================|
    | Data                        |
    |=============================|
    | sizeof(struct Message Type) |
    |=============================|

    Dynamic Message Typed Data (UNDEFINED)
    |=======|====================|
    | Size  | Data               |
    |=======|====================|
    | 1 BIT | $(Size)            |
    |=======|====================|

    Preable         - An array of bits indicating begining of message, usualy equal to {0xAA, 0xAA}
    Control Sum     - Count of set bits in $(Message Type) plus $(Message Data)
    Messsage Type   - Enum value that acts as ID of command behind message
    Message Data    - Data represented in struct that is needed for command execution
*/

/*
===============================================================================
    DEFINED CONSTANTS
===============================================================================
*/

#define CONTROL_SUM_INDEX       ( COUNT_CONTENT(PREAMBLE_ARRAY_CONTENT) )
#define MESSAGE_TYPE_INDEX      ( CONTROL_SUM_INDEX   + sizeof(ControlSumType) )
#define MESSAGE_DATA_INDEX      ( MESSAGE_TYPE_INDEX  + sizeof(SerializedMessageTypeType) )

#define PREAMBLE_SIZE           ( COUNT_CONTENT(PREAMBLE_ARRAY_CONTENT) )
#define VALIDATORS_SIZE         ( PREAMBLE_SIZE + sizeof(ControlSumType) )
#define HEAD_SIZE               ( VALIDATORS_SIZE     + sizeof(SerializedMessageTypeType) )
#define MAX_MESSAGE_SIZE        ( HEAD_SIZE           + sizeof(union AllPcToBoardMessagesUnion) )

#define PREAMBLE_ARRAY_CONTENT  0xAA, 0xAA
#define MESSAGE_PING_CONTENT    "HELLO FROM THE OTHER SIDE!"

#define EISAPP_TIMEOUT_MILISECONDS          5000
#define CVAPP_ADD_TIMEOUT_MILISECONDS       5000
#define RAMP_MAX_STEPS          512

/*
===============================================================================
    UNIVERSAL TYPES
===============================================================================
*/

typedef uint16_t ControlSumType;
typedef uint8_t SerializedMessageTypeType;
typedef uint8_t MessageByteType;

/*
===============================================================================
    ENUM MESSAGES PC TO BOARD
===============================================================================
*/

enum EPcToBoardMessage
{
    EPcToBoardMessagePing = 1,
    EPcToBoardMessageConfigureSweep,
    EPcToBoardMessageConfigureRamp,
    EPcToBoardMessageRunEISApp,
    EPcToBoardMessageRunCVApp
};

/*
===============================================================================
    ENUM MESSAGES BOARD TO PC
===============================================================================
*/

enum EBoardToPcMessage
{
    EBoardToPcMessagePing = 1,
    EBoardToPcMessageStatus,
    EBoardToPcMessageEISMeasurement,
    EBoardToPcMessageCVMeasurement
};

/*
===============================================================================
    UNIVERSAL MESSAGES
===============================================================================
*/

#pragma pack(push, 1)
struct MessagePing
{
    char        message[sizeof(MESSAGE_PING_CONTENT)];
};
#pragma pack(pop)

/*
===============================================================================
    MESSAGES PC TO BOARD
===============================================================================
*/

#pragma pack(push, 1)
struct MessagePcToBoardConfigureSweep
{
    float       sweepStartFreqency;
    float       sweepStopFreqency;
	uint32_t    sweepPoints;
    bool        sweepEnable;
    bool        sweepLogarythmic;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct MessagePcToBoardConfigureRamp
{
    uint32_t    rampStepNumber;
    float       rampStartMiliVolt;
    float       rampPeakMiliVolt;
    float       rampVzeroStart;
    float       rampVzeroPeak;
    float       rampDuration;
    float       rampSampleDelay;
    bool        rampBothDirections;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct MessagePcToBoardRunEISApp
{
    bool        doRun;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct MessagePcToBoardRunCVApp
{
    bool        doRun;
};
#pragma pack(pop)

/*
===============================================================================
    MESSAGES BOARD TO PC
===============================================================================
*/

#pragma pack(push, 1)
struct MessageBoardToPcStatus
{
    uint8_t     status;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct MessageBoardToPcEISMeasurement
{
    float       frequency;
    float       phase;
    float       magnitude;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct MessageBoardToPcCVMeasurement
{
    float       mVVoltage;
    float       mACurrent;
};
#pragma pack(pop)

/*
===============================================================================
    MESSAGE UNION PC TO BOARD
===============================================================================
*/

union AllPcToBoardMessagesUnion
{
    struct MessagePing                              ping;
    struct MessagePcToBoardConfigureSweep           configureSweep;
    struct MessagePcToBoardConfigureRamp            configureRamp;
    struct MessagePcToBoardRunEISApp                runEISApp;
    struct MessagePcToBoardRunCVApp                 runCVApp;

    // TODO add each new Message struct here to find the biggest message
};

/*
===============================================================================
    MESSAGE UNION BOARD TO PC
===============================================================================
*/

union AllBoardToPcMessagesUnion
{
    struct MessagePing                              ping;
    struct MessageBoardToPcStatus                   status;
    struct MessageBoardToPcEISMeasurement           EISMeasurement;
    struct MessageBoardToPcCVMeasurement            CVMeasurement;

    // TODO add each new Message struct here to find the biggest message
};

/*
===============================================================================
    BOARD STATUS ENUM
===============================================================================
*/

enum EBoardStatus
{
    EBoardStatusOK = 1,
    EBoardStatusTimeout,
    EBoardStatusWakeUp,
    EBoardStatusBadRequest,
};

#endif // CONFIGURATION_H

