#ifndef UTILITIES_INCLUDED
#define UTILITIES_INCLUDED

#include <assert.h>
#include <stddef.h>
#include <stdint.h>

#include "Macros.h"

static CONSTEXPR_T uint_fast16_t calculateControlSum(const uint8_t* messageBegin, size_t messageSize) //TODO ask Mr Maciek about the best type choosing and type conversions
{
    const CONSTEXPR_OR_STATIC uint_fast8_t setBitTable[] = {
        0,1,1,2, 1,2,2,3, 1,2,2,3, 2,3,3,4,
        1,2,2,3, 2,3,3,4, 2,3,3,4, 3,4,4,5,
        1,2,2,3, 2,3,3,4, 2,3,3,4, 3,4,4,5,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        1,2,2,3, 2,3,3,4, 2,3,3,4, 3,4,4,5,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        3,4,4,5, 4,5,5,6, 4,5,5,6, 5,6,6,7,
        1,2,2,3, 2,3,3,4, 2,3,3,4, 3,4,4,5,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        3,4,4,5, 4,5,5,6, 4,5,5,6, 5,6,6,7,
        2,3,3,4, 3,4,4,5, 3,4,4,5, 4,5,5,6,
        3,4,4,5, 4,5,5,6, 4,5,5,6, 5,6,6,7,
        3,4,4,5, 4,5,5,6, 4,5,5,6, 5,6,6,7,
        4,5,5,6, 5,6,6,7, 5,6,6,7, 6,7,7,8
    };

    uint_fast16_t sum = 0;
    for (uint_fast16_t i = 0; i < messageSize; ++i)
    {
        sum += setBitTable[messageBegin[i]];
    }
    return sum;
}

#endif
